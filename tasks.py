from query import query
import re, itertools, httpagentparser
from heapq import nlargest; from operator import itemgetter
class tasks:
    def allCountryContinent(args):
        CountryContinentArry=[]
        CountryContinentDict={ 'AF' : 'Asia', 'AX' : 'Europe', 'AL' : 'Europe', 'DZ' : 'Africa', 'AS' : 'Oceania', 'AD' : 'Europe', 'AO' : 'Africa', 'AI' : 'North America', 'AQ' : 'Antarctica', 'AG' : 'North America', 'AR' : 'South America', 'AM' : 'Asia', 'AW' : 'North America', 'AU' : 'Oceania', 'AT' : 'Europe', 'AZ' : 'Asia', 'BS' : 'North America', 'BH' : 'Asia', 'BD' : 'Asia', 'BB' : 'North America', 'BY' : 'Europe', 'BE' : 'Europe', 'BZ' : 'North America', 'BJ' : 'Africa', 'BM' : 'North America', 'BT' : 'Asia', 'BO' : 'South America', 'BQ' : 'North America', 'BA' : 'Europe', 'BW' : 'Africa', 'BV' : 'Antarctica', 'BR' : 'South America', 'IO' : 'Asia', 'VG' : 'North America', 'BN' : 'Asia', 'BG' : 'Europe', 'BF' : 'Africa', 'BI' : 'Africa', 'KH' : 'Asia', 'CM' : 'Africa', 'CA' : 'North America', 'CV' : 'Africa', 'KY' : 'North America', 'CF' : 'Africa', 'TD' : 'Africa', 'CL' : 'South America', 'CN' : 'Asia', 'CX' : 'Asia', 'CC' : 'Asia', 'CO' : 'South America', 'KM' : 'Africa', 'CD' : 'Africa', 'CG' : 'Africa', 'CK' : 'Oceania', 'CR' : 'North America', 'CI' : 'Africa', 'HR' : 'Europe', 'CU' : 'North America', 'CW' : 'North America', 'CY' : 'Asia', 'CZ' : 'Europe', 'DK' : 'Europe', 'DJ' : 'Africa', 'DM' : 'North America', 'DO' : 'North America', 'EC' : 'South America', 'EG' : 'Africa', 'SV' : 'North America', 'GQ' : 'Africa', 'ER' : 'Africa', 'EE' : 'Europe', 'ET' : 'Africa', 'FO' : 'Europe', 'FK' : 'South America', 'FJ' : 'Oceania', 'FI' : 'Europe', 'FR' : 'Europe', 'GF' : 'South America', 'PF' : 'Oceania', 'TF' : 'Antarctica', 'GA' : 'Africa', 'GM' : 'Africa', 'GE' : 'Asia', 'DE' : 'Europe', 'GH' : 'Africa', 'GI' : 'Europe', 'GR' : 'Europe', 'GL' : 'North America', 'GD' : 'North America', 'GP' : 'North America', 'GU' : 'Oceania', 'GT' : 'North America', 'GG' : 'Europe', 'GN' : 'Africa', 'GW' : 'Africa', 'GY' : 'South America', 'HT' : 'North America', 'HM' : 'Antarctica', 'VA' : 'Europe', 'HN' : 'North America', 'HK' : 'Asia', 'HU' : 'Europe', 'IS' : 'Europe', 'IN' : 'Asia', 'ID' : 'Asia', 'IR' : 'Asia', 'IQ' : 'Asia', 'IE' : 'Europe', 'IM' : 'Europe', 'IL' : 'Asia', 'IT' : 'Europe', 'JM' : 'North America', 'JP' : 'Asia', 'JE' : 'Europe', 'JO' : 'Asia', 'KZ' : 'Asia', 'KE' : 'Africa', 'KI' : 'Oceania', 'KP' : 'Asia', 'KR' : 'Asia', 'KW' : 'Asia', 'KG' : 'Asia', 'LA' : 'Asia', 'LV' : 'Europe', 'LB' : 'Asia', 'LS' : 'Africa', 'LR' : 'Africa', 'LY' : 'Africa', 'LI' : 'Europe', 'LT' : 'Europe', 'LU' : 'Europe', 'MO' : 'Asia', 'MK' : 'Europe', 'MG' : 'Africa', 'MW' : 'Africa', 'MY' : 'Asia', 'MV' : 'Asia', 'ML' : 'Africa', 'MT' : 'Europe', 'MH' : 'Oceania', 'MQ' : 'North America', 'MR' : 'Africa', 'MU' : 'Africa', 'YT' : 'Africa', 'MX' : 'North America', 'FM' : 'Oceania', 'MD' : 'Europe', 'MC' : 'Europe', 'MN' : 'Asia', 'ME' : 'Europe', 'MS' : 'North America', 'MA' : 'Africa', 'MZ' : 'Africa', 'MM' : 'Asia', 'NA' : 'Africa', 'NR' : 'Oceania', 'NP' : 'Asia', 'NL' : 'Europe', 'NC' : 'Oceania', 'NZ' : 'Oceania', 'NI' : 'North America', 'NE' : 'Africa', 'NG' : 'Africa', 'NU' : 'Oceania', 'NF' : 'Oceania', 'MP' : 'Oceania', 'NO' : 'Europe', 'OM' : 'Asia', 'PK' : 'Asia', 'PW' : 'Oceania', 'PS' : 'Asia', 'PA' : 'North America', 'PG' : 'Oceania', 'PY' : 'South America', 'PE' : 'South America', 'PH' : 'Asia', 'PN' : 'Oceania', 'PL' : 'Europe', 'PT' : 'Europe', 'PR' : 'North America', 'QA' : 'Asia', 'RE' : 'Africa', 'RO' : 'Europe', 'RU' : 'Europe', 'RW' : 'Africa', 'BL' : 'North America', 'SH' : 'Africa', 'KN' : 'North America', 'LC' : 'North America', 'MF' : 'North America', 'PM' : 'North America', 'VC' : 'North America', 'WS' : 'Oceania', 'SM' : 'Europe', 'ST' : 'Africa', 'SA' : 'Asia', 'SN' : 'Africa', 'RS' : 'Europe', 'SC' : 'Africa', 'SL' : 'Africa', 'SG' : 'Asia', 'SX' : 'North America', 'SK' : 'Europe', 'SI' : 'Europe', 'SB' : 'Oceania', 'SO' : 'Africa', 'ZA' : 'Africa', 'GS' : 'Antarctica', 'SS' : 'Africa', 'ES' : 'Europe', 'LK' : 'Asia', 'SD' : 'Africa', 'SR' : 'South America', 'SJ' : 'Europe', 'SZ' : 'Africa', 'SE' : 'Europe', 'CH' : 'Europe', 'SY' : 'Asia', 'TW' : 'Asia', 'TJ' : 'Asia', 'TZ' : 'Africa', 'TH' : 'Asia', 'TL' : 'Asia', 'TG' : 'Africa', 'TK' : 'Oceania', 'TO' : 'Oceania', 'TT' : 'North America', 'TN' : 'Africa', 'TR' : 'Asia', 'TM' : 'Asia', 'TC' : 'North America', 'TV' : 'Oceania', 'UG' : 'Africa', 'UA' : 'Europe', 'AE' : 'Asia', 'GB' : 'Europe', 'US' : 'North America', 'UM' : 'Oceania', 'VI' : 'North America', 'UY' : 'South America', 'UZ' : 'Asia', 'VU' : 'Oceania', 'VE' : 'South America', 'VN' : 'Asia', 'WF' : 'Oceania', 'EH' : 'Africa', 'YE' : 'Asia', 'ZM' : 'Africa', 'ZW' : 'Africa' }
        queryA="visitor_country"
        queryData=query.queryJSON(args, queryA, None)
        for e in queryData:
            if e in CountryContinentDict:
                item=CountryContinentDict[e]
            else:
                item="Undefined"
            if args.verbose:
                print("Continent: ["+item+']')
            CountryContinentArry.append(item)
        return CountryContinentArry

    def mainBrowserName(args):
        mainBrowserNameArry=[]
        queryA="visitor_useragent"
        queryData=query.queryJSON(args, queryA, None)
        for e in queryData:
            parser=httpagentparser.detect(e)
            if "browser" in parser:
                item=parser["browser"]["name"]
            else:
                item="Undefined"
            if args.verbose:
                print("Main Browser Name: ["+item+']')
            mainBrowserNameArry.append(item)
        return mainBrowserNameArry

    def topTenReaders(args):
        visitorReadtime={}
        queryA="visitor_uuid"
        queryB="event_readtime"
        queryData=query.queryJSON(args, queryA, queryB)
        for e in queryData:
            UserID, Readtime=e.split(' ')
            if Readtime=="None":
                Value=0
            else:
                Value=int(Readtime)
            if UserID in visitorReadtime:
                visitorReadtime[UserID]+=Value
            else:
                visitorReadtime[UserID]=Value
        TopTen=dict(nlargest(10, visitorReadtime.items(), key=itemgetter(1)))
        if args.verbose:
            print ("Top 10 Readers: ["+str(TopTen)+']')
        return TopTen

    def docAlsoLikedBy(args, doc):
        queryA="visitor_uuid"
        queryB="env_doc_id"
        UserList=[]
        queryData=query.queryJSON(args, queryA, queryB)
        for e in queryData:
            UserID, DocID=e.split(' ')
            if DocID==doc and DocID!=None and UserID not in UserList:
                if args.verbose:
                    print(UserID)
                DocList.append(UserID)
        return UserList

    def userAlsoLiked(args, user):
        queryA="visitor_uuid"
        queryB="env_doc_id"
        DocList=[]
        queryData=query.queryJSON(args, queryA, queryB)
        for e in queryData:
            UserID, DocID=e.split(' ')
            if UserID==user and UserID!=None and DocID not in DocList:
                if args.verbose:
                    print(DocID)
                DocList.append(DocID)
        return DocList

    def alsoLikes(args):
        queryA="visitor_uuid"
        queryB="env_doc_id"
        alsoLikesDict={}
        queryData=query.queryJSON(args, queryA, queryB)
        for e in queryData:
            UserID, DocID=e.split(' ')
            if DocID==args.doc and DocID != None and UserID not in alsoLikesDict:
                alsoLikesDict[UserID]=tasks.userAlsoLiked(args, UserID)
                if args.verbose:
                    print(UserID+" likes "+str(alsoLikesDict[UserID]))
        return alsoLikesDict

    def topTenAlsoLikes(args, ID):
        queryA="visitor_uuid"
        queryB="env_doc_id"
        queryData=query.queryJSON(args, queryA, queryB)
        for e in queryData:
            UserID, DocID=e.split(' ')
            if UserID==ID:
                print()
            elif DocID==ID:
                print()
                