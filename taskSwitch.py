from query import query; from graph import graph; from tasks import tasks
import time
class taskSwitch:
    def task_2a(args):
        
        queryData=query.queryJSON(args, "visitor_country", None)
        print(queryData)
        graph.plot(args, queryData, "Country Code", "Frequency")
    def task_2b(args):
        data=tasks.allCountryContinent(args)
        #print(data)
        graph.plot(args, tasks.allCountryContinent(args), "Continent", "Frequency")
    def task_3a(args):
        queryData=query.queryJSON(args, "visitor_useragent", None)
        graph.plot(args, queryData, "Verbose User Agent", "Frequency")
    def task_3b(args):
        data=tasks.mainBrowserName(args)
        graph.plot(args, data, "Main Browser Type", "Frequency")
    def task_4(args):
        data=tasks.topTenReaders(args)
        graph.plot(args, data, "Readtime", "Visitor UUID")
    def task_5a(args):
        data=tasks.docAlsoLikedBy(args, args.doc)
        print(data)
    def task_5b(args):
        data=tasks.userAlsoLiked(args, args.user)  
        print(data)
    def task_5c(args):
        data=tasks.alsoLikes(args)
        print(data)
    def task_5d(args):
        print("Task 5d")
    def task_6(args):
        print("Task 6")
    def task_7(args):
        print("Task 7")
    def run(self, args):
        start=time.time()
        print(time.strftime('%H:%M:%S', time.gmtime()))
        if args.task=="2a":
            self.task_2a(args)
        elif args.task=="2b":
            self.task_2b(args)
        elif args.task=="3a":
            self.task_3a(args)
        elif args.task=="3b":
            self.task_3b(args)
        elif args.task=="4":
            self.task_4(args)
        elif args.task=="5a":
            self.task_5a(args)
        elif args.task=="5b":
            self.task_5b(args)
        elif args.task=="5c":
            self.task_5c(args)
        elif args.task=="5d":
            self.task_5d(args)
        elif args.task=="6":
            self.task_6(args)
        elif args.task=="7":
            self.task_7(args)
        end=time.time()
        print(end-start)
        
