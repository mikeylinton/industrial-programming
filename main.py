import argparse
from taskSwitch import taskSwitch as task; from gui import tKgui
def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-u", "--user", help="User UUID")
    parser.add_argument("-d", "--doc", help="Document UUID")
    parser.add_argument("-t", "--task", help="Task ID")
    parser.add_argument("-f", "--file", help="JSON file")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    args=parser.parse_args()
    if args.task=="7":
        tKgui.startGUI(tKgui, args)
    else:
        task.run(task, args)
    

if __name__ == '__main__':
    main()
    exit(0)
#python3 main.py -f sample-big.json -d 140204115519-f5fa6ce8b288c9f10e0c8bc7e1a456a0 -u 2f63e0cca690da91 -t 5