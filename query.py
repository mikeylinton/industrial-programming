import json, sys, os
class query:
    def queryJSON(args, queryA, queryB):
        list=[]
        with open(args.file, 'r', encoding='utf-8') as f:
            e = f.readline()
            while e:
                if e.strip():
                    j = json.loads(e)
                    try:
                        itemA=j[queryA]
                    except:
                        itemA=None
                    if not queryB == (None):
                        try:
                            itemB=j[queryB]
                        except:
                            itemB=None
                        if args.verbose:
                            print("Query ["+queryA+"]: "+str(itemA))
                            print("Query ["+queryB+"]: "+str(itemB))
                            print()
                        list.append(str(itemA)+' '+str(itemB))
                    else:
                        if args.verbose:
                            print("Query ["+queryA+"]: "+str(itemA))
                        list.append(str(itemA))
                e = f.readline()
        return list