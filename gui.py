from taskSwitch import taskSwitch as task
from tkinter import *
class tKgui:

    def exit():
           exit(0)
    def task2a(args):
        args.task="2a"
        task.task_2a(args)
    def task2b(args):
        args.task="2b"
        task.task_2b(args)
    def task3a(args):
        args.task="3a"
        task.task_3a(args)
    def task3b(args):
        args.task="3b"
        task.task_3b(args)
    def task4(args):
        args.task="4"
        task.task_4(args)
    def task5a(args):
        args.task="5a"
        task.task_5a(args)
    def task5b(args):
        args.task="5b"
        task.task_5b(args)
    def task5c(args):
        args.task="5c"
        task.task_5c(args)
    def task5d(args):
        args.task="5d"
        task.task_5d(args)
    def task6(args):
        args.task="6"
        task.task_6(args)

    def startGUI(self, args):
        top=Tk()
        size=1
        minY=350
        minX=int(minY/2.25)
        maxX=int(minX*size)
        maxY=int(minY*size)
        top.geometry(str(maxX)+'x'+str(maxY))
        exitButton=Button(top,text="Exit",command=self.exit)
        exitButton.place(x=maxX*0.6,y=maxY*0.08)
        task2aButton=Button(top,text="Task 2a",command=lambda:self.task2a(args))
        task2aButton.place(x=maxX*0.1,y=maxY*0.08)
        task2bButton=Button(top,text="Task 2b",command=lambda:self.task2b(args))
        task2bButton.place(x=maxX*0.1,y=maxY*0.08*2)
        task3aButton=Button(top,text="Task 3a",command=lambda:self.task3a(args))
        task3aButton.place(x=maxX*0.1,y=maxY*0.08*3)
        task3bButton=Button(top,text="Task 3b",command=lambda:self.task3b(args))
        task3bButton.place(x=maxX*0.1,y=maxY*0.08*4)
        task4Button=Button(top,text="Task 4",command=lambda:self.task4(args))
        task4Button.place(x=maxX*0.1,y=maxY*0.08*5)
        task5aButton=Button(top,text="Task 5a",command=lambda:self.task5a(args))
        task5aButton.place(x=maxX*0.1,y=maxY*0.08*6)
        task5bButton=Button(top,text="Task 5b",command=lambda:self.task5b(args))
        task5bButton.place(x=maxX*0.1,y=maxY*0.08*7)
        task5cButton=Button(top,text="Task 5c",command=lambda:self.task5c(args))
        task5cButton.place(x=maxX*0.1,y=maxY*0.08*8)
        task5dButton=Button(top,text="Task 5d",command=lambda:self.task5d(args))
        task5dButton.place(x=maxX*0.1,y=maxY*0.08*9)
        task6Button=Button(top,text="Task 6",command=lambda:self.task6(args))
        task6Button.place(x=maxX*0.1,y=maxY*0.1*8)
        top.mainloop()
            