import matplotlib
import matplotlib.pyplot as pyplot
class graph:
    def plot(args, data, xlabel, ylabel):
        matplotlib.use("TkAgg")
        pyplot.title("Task "+args.task)
        pyplot.xlabel(xlabel)
        pyplot.ylabel(ylabel)
        if isinstance(data, dict):
            x=list(data.keys())
            y=list(data.values())
            pyplot.barh([i for i in x], [j for j in y])
        if isinstance(data, list):
            x=data
            y=len(list(set(data)))
            pyplot.hist([i for i in x], y)
        print(type(data))
        pyplot.show()